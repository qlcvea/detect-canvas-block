# detect-canvas-block

Detect if data extraction from HTML canvas elements is blocked.

---

## Usage when included with a `<script>` tag

```javascript
if (detectCanvasBlock()) {
  // canvas data extraction is allowed
} else {
  // canvas data extraction is blocked
}
```

## Usage when included as a module

```javascript
const detectCanvasBlock = require('detect-canvas-block');

if (detectCanvasBlock()) {
  // canvas data extraction is allowed
} else {
  // canvas data extraction is blocked
}
```