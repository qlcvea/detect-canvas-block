function detectCanvasBlock() {
  var canvas = document.createElement('canvas');
  canvas.width = 75;
  canvas.height = 75;
  var ctx = canvas.getContext('2d');
  ctx.beginPath();
  ctx.rect(10,10,50,50);
  ctx.stroke();
  var url1 = canvas.toDataURL();
  ctx.clearRect(0, 0, canvas.width, canvas.height);
  ctx.beginPath();
  ctx.rect(30,30,25,25);
  ctx.stroke();
  var url2 = canvas.toDataURL();
  return !(url1 == url2);
}

if (typeof module == 'object') {
  module.exports = detectCanvasBlock;
}